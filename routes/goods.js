'use strict';
var router = require('express').Router();
var AV = require('leanengine');
var moment = require('../util/date');
var pageSize = require('../util/config').pageSize;

var Goods = AV.Object.extend('Goods');

// 商品列表-页面
router.get('/list', function(req, res, next) {
    var query = new AV.Query(Goods);
    query.count().then(total => {
        res.render('goods/list', {
            total: total,
            pageSize: pageSize,
            pageIndex: 0,
            originalUrl: req.originalUrl
        });
    });
});

// 商品列表-接口
router.post('/list', function(req, res, next) {
    var pageIndex = req.body.pageIndex;
    // console.log(pageIndex);
    var query = new AV.Query(Goods);
    query.include('category');
    query.descending('createdAt');
    query.limit(pageSize);
    query.skip(pageSize * pageIndex);
    query.find().then(function(results) {
        res.send(results.map(item => {
            item.createdAt = moment.date(item.createdAt);
            return item;
        }));
    }, function(err) {
        if (err.code === 101) {
            // 该错误的信息为：{ code: 101, message: 'Class or object doesn\'t exists.' }，说明 Todo 数据表还未创建，所以返回空的 Todo 列表。
            // 具体的错误代码详见：https://leancloud.cn/docs/error_code.html
            res.render('goods/list', {
                title: 'TODO 列表',
                goods: []
            });
        } else {
            next(err);
        }
    }).catch(next);
});

// 添加商品页面
router.get('/add', function(req, res, next) {
    res.render('goods/add', {
        originalUrl: req.originalUrl
    });
});


// 编辑时读取商品数据
router.post('/find', function(req, res, next) {
    var query = new AV.Query(Goods);
    query.get(req.body.objectId).then(goods => {
        goods.set('category', goods.get('category').get('objectId'))
        res.send(goods);
    });
});

// 添加商品接口
router.post('/add', function(req, res, next) {
    var goods = new Goods;
    var form = req.body;
    /*各种数据转化*/
    // 价格格式化
    form.price = Number.parseFloat(form.price);
    // bool 格式化
    form.isHot = form.isHot === 'true' ? true : false;
    form.isNew = form.isNew === 'true' ? true : false;
    console.log(form);
    // 生成分类对象
    form.category = AV.Object.createWithoutData('Category', form.category);
    // 转化upload带来的图片
    form.images = form.images.map(item => {
        return item.url;
    });
    form.detail = form.detail.map(item => {
        return item.url;
    });
    /*转化结束*/
    if (form.objectId) {
        // 查出原对象
        goods = AV.Object.createWithoutData('Goods', form.objectId);
        delete form.objectId;
        delete form.createdAt;
        delete form.updatedAt;
    }
    goods.save(form).then(goods => {
        // console.log(goods);
        res.send(goods);
    }, err => {
        console.log(err);
        res.send(err);
    });

});

// 删除商品接口
router.post('/delete', function (req, res, next) {
    var objectId = req.body.objectId;
    var goods = AV.Object.createWithoutData('Goods', objectId);
    goods.destroy().then(result => {
        res.send(result);
    }, err => {
        res.send(err);
    })
});


module.exports = router;